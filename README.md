# Example API

A boilerplate TODO API.

# Usage
1. Apply Terraform from `.aws/terraform` to create the AWS resources.
1. Create webhook signing key in Contentful.
1. Add the secret key to the AWS secret value created by Terraform.
1. Create a new Contentful webhook with the URL of the API Gateway endpoint (see TF output).  The full URL is: `POST {gateway_base_url}/trigger`.