import assert from 'node:assert/strict';
import {
    signRequest,
    type CanonicalRequest
} from '@contentful/node-apps-toolkit'


const { SIGNING_SECRET, WEBHOOK_ENDPOINT } = process.env
assert(SIGNING_SECRET, 'SIGNING_SECRET is not set')
assert(WEBHOOK_ENDPOINT, 'WEBHOOK_ENDPOINT is not set')

console.log('Generating signed request...')

// This mocks the webhook request from Contentful before it is signed.
const canonicalRequest: CanonicalRequest = {
    // the backend authorizer will check for this exact path
    path: '/contentful-signed-requests/trigger',
    // as from webhook request log - all x-contentful-* headers
    headers: {
        'content-type': 'application/json',
        'x-contentful-crn': 'crn:v1:contentful:space:xxxxxxxxx:environment:master',
        'x-contentful-event-datetime': new Date().toISOString(),
        'x-contentful-idempotency-key': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
        'x-contentful-topic': 'ContentManagement.Entry.publish',
        'x-contentful-webhook-name': 'test',
        'x-contentful-webhook-request-attempt': '1',
        "x-contentful-timestamp": new Date().getTime().toString()
    },
    method: 'POST',
    // mock payload
    body: '{"sys":{"id":"6j3J7JwZ5Mzv7Z7Z7Z7Z7Z","type":"Entry","space":{"sys":{"type":"Link","linkType":"Space","id":"6y40a2vz4r1c"},"id":"6y40a2vz4r1c"},"environment":{"sys":{"type":"Link","linkType":"Environment","id":"master"},"id":"master"},"createdAt":"2021-10-18T14:21:00Z","updatedAt":"2021-10-18T14:21:00Z","publishedAt":"2021-10-18T14:21:00Z","contentType":{"sys":{"type":"Link","linkType":"ContentType","id":"test"},"id":"test"},"revision":1},"fields":{"title":{"en-US":"Test"}}}'
}

const requestSignature = signRequest(
    SIGNING_SECRET!,
    canonicalRequest
)

// This is the signed request that will be sent to the API endpoint.
const signedRequest =  {
    method: canonicalRequest.method,
    headers: {
        ...canonicalRequest.headers,
        ...requestSignature
    },
    body: canonicalRequest.body
}

console.log(JSON.stringify(signedRequest, null, 2))

console.log('Testing API endpoint...')

fetch(WEBHOOK_ENDPOINT!, signedRequest)
    .then(response => response.json())
    .then(data => {
        console.log('Success:', data);
    })
    .catch((error) => {
        console.error('Error:', error);
    });
