terraform {
    backend "s3" {
        bucket = "tf-states-us-east-1-bgoodman"
        key    = "contentful-signed-requests/terraform.tfstate"
        region = "us-east-1"
    }
}