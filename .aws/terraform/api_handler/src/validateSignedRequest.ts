import { verifyRequest } from '@contentful/node-apps-toolkit'
import { SecretsManagerClient, GetSecretValueCommand } from '@aws-sdk/client-secrets-manager'
import { APIGatewayProxyEvent } from 'aws-lambda'

const REQUEST_TTL = 30

const SECRET_NAME = process.env.SECRET_NAME
const client = new SecretsManagerClient()

export const isSignedRequestValid = async (event: APIGatewayProxyEvent) => {

    if (!SECRET_NAME) {
        throw new Error('SECRET_NAME is not set')
    }

    const secret =(await client.send(
        new GetSecretValueCommand({
        SecretId: SECRET_NAME,
        VersionStage: 'AWSCURRENT',
    }))).SecretString

    if (!secret) {
        throw new Error('Secret not found')
    }

    const body = event.body && event.isBase64Encoded
        ? Buffer.from(event.body!, 'base64').toString('utf-8')
        : (event.body || undefined)

    const canonicalRequest = {
        // i.e., '/contentful-signed-requests/trigger'
        path: event.path,
        // as from request - all x-contentful-* headers:
        // 'content-type,x-contentful-crn,x-contentful-event-datetime,x-contentful-idempotency-key,x-contentful-signed-headers,x-contentful-timestamp,x-contentful-topic,x-contentful-webhook-name,x-contentful-webhook-request-attempt'
        headers: event.headers,
        method: event.httpMethod,
        body: body,
    }

    try {
        const isValid = verifyRequest(secret, canonicalRequest as any, REQUEST_TTL)
        console.log('isValid', isValid)
        return isValid
      } catch (e) {
        console.error(e);
        throw e
      }

}