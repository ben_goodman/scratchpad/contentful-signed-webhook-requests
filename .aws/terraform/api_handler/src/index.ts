import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'
import { isSignedRequestValid } from './validateSignedRequest'

export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    console.log('event', event)
    const  { resource, httpMethod } = event

    if (httpMethod === 'GET' && resource === '/health') {
        return {
            headers: {"Content-Type": "application/json"},
            statusCode: 200,
            body: JSON.stringify({ message: 'Ok' }),
        }
    }


    // POST /trigger
    if (httpMethod === 'POST' && resource === '/trigger') {

        try {
            const isValid = await isSignedRequestValid(event)

            if ( isValid ) {
                return {
                    headers: {"Content-Type": "application/json"},
                    statusCode: 200,
                    body: JSON.stringify({ message: 'OK' }),
                }
            } else {
                return {
                    headers: {"Content-Type": "application/json"},
                    statusCode: 401,
                    body: JSON.stringify({ message: 'Unauthorized' }),
                }
            }
        } catch (error) {
            return {
                headers: {"Content-Type": "application/json"},
                statusCode: 500,
                body: JSON.stringify({ error }),
            }
        }
    }

    // default response
    return {
        headers: {"Content-Type": "application/json"},
        statusCode: 404,
        body: JSON.stringify({ message: 'Resource not found' }),
    }
}
