output "gateway_base_url" {
    value = aws_apigatewayv2_stage.default.invoke_url
}
