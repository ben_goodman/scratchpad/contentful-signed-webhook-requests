resource "aws_apigatewayv2_api" "default" {
    name          = "${var.project_name}-api-gateway"
    protocol_type = "HTTP"
}

resource "aws_cloudwatch_log_group" "default" {
    name = "/aws/${var.project_name}_gateway/${aws_apigatewayv2_api.default.name}"
    retention_in_days = 30
}

resource "aws_apigatewayv2_stage" "default" {
    api_id = aws_apigatewayv2_api.default.id
    name        = "${var.project_name}"
    auto_deploy = true

    access_log_settings {
        destination_arn = aws_cloudwatch_log_group.default.arn

        format = jsonencode({
            requestId               = "$context.requestId"
            sourceIp                = "$context.identity.sourceIp"
            requestTime             = "$context.requestTime"
            protocol                = "$context.protocol"
            httpMethod              = "$context.httpMethod"
            resourcePath            = "$context.resourcePath"
            routeKey                = "$context.routeKey"
            status                  = "$context.status"
            responseLength          = "$context.responseLength"
            integrationErrorMessage = "$context.integrationErrorMessage"
            authorizerErrorMessage  = "$context.authorizer.error"
        })
    }
}

resource "aws_apigatewayv2_integration" "default" {
    api_id = aws_apigatewayv2_api.default.id
    integration_uri    = module.api_lambda_function.invoke_arn
    integration_type   = "AWS_PROXY"
    integration_method = "POST"
}

resource "aws_apigatewayv2_route" "health_check" {
    api_id = aws_apigatewayv2_api.default.id
    target    = "integrations/${aws_apigatewayv2_integration.default.id}"
    route_key = "GET /health"
}

resource "aws_apigatewayv2_route" "post_webhook" {
    api_id = aws_apigatewayv2_api.default.id
    target    = "integrations/${aws_apigatewayv2_integration.default.id}"
    route_key = "POST /trigger"
}

