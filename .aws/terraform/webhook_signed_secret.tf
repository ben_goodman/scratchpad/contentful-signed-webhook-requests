resource "aws_secretsmanager_secret" "webhook_signed_secret" {
    name = "${var.project_name}-webhook-signed-secret-${terraform.workspace}"
}
